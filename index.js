/** @format */

import {AppRegistry} from 'react-native';
import App from './App';

AppRegistry.registerComponent("hr_app", () => App);
