import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native'
import { withNavigation } from 'react-navigation'
import { normalize } from '../function/nomalize';
import color from '../color'
import { Fonts } from '../resource/Fonst';


export const HeaderBack = withNavigation(({ navigation }) => {

    return <TouchableOpacity onPress={() => navigation.goBack(null)} style={{ paddingLeft: normalize(10), fontSize: normalize(20) }}>
        <Text>{'กลับ'}</Text>
    </TouchableOpacity>
})

export const HeaderTitle = ({ title }) => {
    return <View style={{ flex: 1 }}>
        <Text adjustsFontSizeToFit style={{ fontSize: normalize(20), color: color.blue, fontFamily: Fonts.MaliRegular, alignSelf: 'center', textAlign: 'center', alignItems: 'center' }}>{title}</Text>
    </View>
}


export const HeaderRight = () => {
    return <Text style={{ paddingRight: normalize(10), color: color.black }} ></Text>
}



// const TextRight = styled.Text`
//     color: black;
//     background-color: yellow;
//     ${props => props.disabled && `
//         color: gray;
//         background-color: red;
//         font-size: ${normalize(30)}
//     `}
// `