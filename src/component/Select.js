import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput } from 'react-native'
import RNPickerSelect from 'react-native-picker-select';
import color from '../color'
import { normalize } from '../function/nomalize';
import { Fonts } from '../resource/Fonst';

export default class Select extends Component {
    constructor(props) {
        super(props);

        this.inputRefs = {};

        this.state = {
            favColor: undefined,

            favSport: undefined,
            favSport2: undefined,
            Leave_type: [
                {
                    label: 'Fulltime',
                    value: 'Fulltime',
                },
                {
                    label: 'Hafttime',
                    value: 'Hafttime',
                },

            ],
        };
    }


    render() {
        return (
            <View>
                <View style={{ width: normalize(300), alignItems: 'center' }}>
                    {/* <Text>Name?</Text>
                    <TextInput
                        ref={(el) => {
                            this.inputRefs.name = el;
                        }}
                        returnKeyType="next"
                        enablesReturnKeyAutomatically
                        onSubmitEditing={() => {
                            this.inputRefs.picker.togglePicker();
                        }}
                        style={pickerSelectStyles.inputIOS}
                        blurOnSubmit={false}
                    /> */}
                    <RNPickerSelect
                        placeholder={{
                            label: 'Select a color...',
                            value: null,
                            color: '#9EA0A4',
                        }}
                        items={this.state.Leave_type}
                        onValueChange={(value) => {
                            this.setState({
                                favColor: value,
                            });
                        }}
                        onUpArrow={() => {
                            this.inputRefs.name.focus();
                        }}
                        onDownArrow={() => {
                            this.inputRefs.picker2.togglePicker();
                        }}
                        style={{ ...pickerSelectStyles }}
                        value={this.state.favColor}
                        ref={(el) => {
                            this.inputRefs.picker = el;
                        }}
                    />




                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {

        backgroundColor: color.white,
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: normalize(16),
        paddingTop: normalize(13),
        paddingHorizontal: normalize(10),
        paddingBottom: normalize(12),
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
        fontFamily: Fonts.MaliRegular,
        alignItems: 'center'

    },
    inputAndroid: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});
