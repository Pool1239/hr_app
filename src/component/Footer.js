import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import color from '../color'
import { normalize } from '../function/nomalize'
import { Fonts } from '../resource/Fonst';

export default class Footer extends Component {
    render() {
        return (
            <View>
                <View style={{ flex: 1 }}>

                    <View style={{ position: 'absolute', left: 0, right: 0, bottom: 0, alignItems: 'center', backgroundColor: color.white, height: normalize(42), padding: 10 }}>
                        <Text style={{ color: color.bluesky, fontFamily: Fonts.MaliRegular }}> Copyright Officeone (Thailand)Co.Ltd</Text>


                    </View>
                </View>
            </View>
        )
    }
}
