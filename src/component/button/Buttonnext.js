import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native'
import color from '../../color';
import { normalize } from '../../function/nomalize';
import icon from '../../resource/icon';
import { Fonts } from '../../resource/Fonst';

export default class Buttonnext extends Component {
    render() {
        return (
            <View>
                <View >
                    <TouchableOpacity style={styles.sign_button} onPress={() => this.props.onLogin()}>
                        <View style={{ display: 'flex', flexDirection: 'row' }}>
                            <Text style={styles.textsign_button}>ตกลง</Text>
                            {/* <Image source={icon.pointright} style={{ width: normalize(20), height: normalize(20), marginTop: 5, marginLeft: 10 }}></Image> */}
                        </View>
                    </TouchableOpacity>

                </View>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    sign_button: {
        fontSize: normalize(30),
        width: normalize(330),
        height: normalize(45),
        padding: normalize(5),
        alignItems: 'center',
        borderRadius: normalize(10),
        margin: normalize(10),
        backgroundColor: color.next,

    },
    textsign_button: {
        fontSize: normalize(20),
        color: color.white,
        fontFamily: Fonts.MaliRegular
    },
    textlogin_button: {
        fontSize: normalize(20),
        color: color.bluesky,
        fontFamily: Fonts.MaliRegular


    },
})