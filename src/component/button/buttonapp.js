import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet, Image } from 'react-native'
import color from '../../color';
import { normalize } from '../../function/nomalize';
import icon from '../../resource/icon'
import { Fonts } from '../../resource/Fonst';

export default class Buttonapp extends Component {

  constructor(props) {
    super(props)

    this.state = {

    }
  }

  componentDidMount = () => {
    console.log("props", this.props)
  }

  render() {
    return (
      <View>
        <TouchableOpacity style={styles.back_button} onPress={() => this.props.onBack()}>
          <View  >

            <Text style={styles.textback_button}>ย้อนกลับ</Text>

          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({

  back_button: {
    fontSize: normalize(30),
    width: normalize(330),
    height: normalize(45),
    padding: normalize(5),
    alignItems: 'center',
    borderRadius: normalize(10),
    backgroundColor: color.back,
    margin: normalize(10),



  },
  textsign_button: {
    fontSize: normalize(30),
    color: color.white,
    fontFamily: Fonts.MaliRegular
  },
  textback_button: {
    fontSize: normalize(20),
    color: color.white,
    fontFamily: Fonts.MaliRegular


  },
})


