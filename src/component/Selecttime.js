import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment'

export default class Selecttime extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isDateTimePickerVisible: false,
            time: moment().format('LTS'),
            timetwo: new Date

        }
    }
    componentWillMount() {
        console.log(this.props.setdateTime);

    }


    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log(moment(date).format('LTS'));
        this.props.setdateTime(moment(date).format('LTS'))
        this._hideDateTimePicker();
    };
    render() {
        return (
            <View>
                <View style={{ flex: 1 }}>
                    <TouchableOpacity onPress={this._showDateTimePicker}>
                        <Text>กรุณาเลือกเวลา</Text>
                    </TouchableOpacity>
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        mode="time"
                    />
                </View>
            </View>
        )
    }
}
