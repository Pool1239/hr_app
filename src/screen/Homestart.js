import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, ImageBackground, StyleSheet, Image, StatusBar } from 'react-native'
import image from '../resource/image';
import color from '../color'
import { normalize } from '../function/nomalize';
import { AppRegistry } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation'
import moment from 'moment'
import { POST, LOGIN } from '../service';
import Loginfacebook from './Loginfacebook';
import { Fonts } from '../resource/Fonst'
// import 'moment/locale/th'  
// moment.locale('th')


export default class Homestart extends Component {
    constructor() {

        super()
        this.state = {
            date: moment().format('DD-MM-YYYY'),
            time: new Date()
        }
    }


    _gotologin = () => {
        this.props.navigation.navigate('Login')
    }

    _gotosignup = () => {
        this.props.navigation.navigate('Signup')
    }

    render() {
        return (
            <ImageBackground source={image.backgroundhome} style={{ width: '100%', height: '100%' }}>
                <StatusBar translucent barStyle={'light-content'} backgroundColor={'transparent'} />
                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View style={{ alignItems: 'center', marginTop: normalize(100) }}>
                        <Image style={{ width: normalize(300), height: normalize(100), resizeMode: "contain" }} source={image.logo3}></Image>
                    </View>


                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View >

                            <TouchableOpacity style={styles.sign_button} onPress={() => this._gotosignup()}>
                                <Text style={styles.textsign_button}>SIGN UP</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.login_button} onPress={() => this._gotologin()}>
                                <Text style={styles.textlogin_button}> LOGIN</Text>
                            </TouchableOpacity>
                            {/* <View style={styles.container}>
                                <Loginfacebook />
                            </View> */}
                        </View>
                    </View>

                    <View style={{ marginBottom: normalize(30) }}>
                        <Text style={{ color: color.white, fontSize: normalize(30), fontFamily: Fonts.MaliRegular }}>{moment(this.state.time).format('LT')}</Text>
                        <Text style={{ color: color.white, fontSize: normalize(20), fontFamily: Fonts.MaliRegular }}>{this.state.date}</Text>
                    </View>
                </View>
            </ImageBackground >
        )
    }
}
AppRegistry.registerComponent('AwesomeProject', () => AlignItemsBasics);

const styles = StyleSheet.create({
    sign_button: {
        borderWidth: normalize(1),
        width: normalize(300),
        height: normalize(60),
        padding: 10,
        margin: 10,
        borderRadius: normalize(10),
        alignItems: 'center',
        backgroundColor: color.bluesky
    },
    login_button: {
        borderWidth: normalize(1),
        width: normalize(300),
        height: normalize(60),
        padding: 10,
        margin: 10,
        borderRadius: normalize(10),
        alignItems: 'center',
        backgroundColor: color.white
    },
    textsign_button: {
        fontSize: normalize(22),
        color: color.white,
        fontFamily: Fonts.MaliRegular
    },
    textlogin_button: {
        fontSize: normalize(22),
        color: color.bluesky,
        fontFamily: Fonts.MaliRegular

    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
})
