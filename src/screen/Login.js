import React, { Component } from 'react'
import { StackActions, NavigationActions } from 'react-navigation'
import { Text, Image, StyleSheet, Alert, Modal, TouchableHighlight, TouchableOpacity, ImageBackground, StatusBar, ToolbarAndroid } from 'react-native'
import { View, ScrollView, AppRegistry, TextInput, ActivityIndicator } from 'react-native';
import color from '../color';
import image from '../resource/image';
import { normalize } from '../function/nomalize';
import icon from '../resource/icon'
import { POST, LOGIN } from '../service';
import { Fonts } from '../resource/Fonst'


export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: null,
      username: '',
      password: '',

    };
  }
  testalert() {
    Alert.alert(
      'เข้าสู่ระบบ',
      'ไม่มี Username ชื่อนี้ !!!',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ],
      { cancelable: false },
    );
  }

  _onPressButton = async () => {
    const object = {
      username: this.state.username,
      password: this.state.password
    }

    console.log("object", object)
    try {
      let res = await POST(LOGIN, object)
      console.log(res);
      if (res.success === true) {
        res.result.map((el, i) => {
          this.setState({ empID: el })
        })
        this.props.navigation.navigate('Home', this.state.empID)

      } else {
        this.testalert()
      }

    } catch (error) {
      console.log(error)
    }
  }
  backhomestart = () => {
    let { dispatch } = this.props.navigation;
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: 'Homestart' })
      ]
    })
    dispatch(resetAction)
  }
  render() {
    const inputAccessoryViewID = "uniqueID";
    const { userName, password, phone } = this.state
    return (

      <ImageBackground source={image.backgroundhome} style={{ width: '100%', height: '100%' }}
        blurRadius={2}
      >
        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}>
          <View style={{ alignItems: 'center', marginTop: normalize(50) }}>
            <Image style={{ resizeMode: "contain", width: normalize(300), height: normalize(100) }} source={image.logo3}></Image>
            <Image style={{ resizeMode: "contain", width: normalize(200), height: normalize(120) }} source={icon.muscod}></Image>
          </View>

          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <TextInput placeholder="username"
              name='userName'
              type="text"
              placeholderTextColor={color.white}
              style={styles.textinput}
              value={userName}
              onChangeText={(text) => this.setState({ username: text })}
            >
            </TextInput>
            <TextInput placeholder="Password"
              password={true}
              secureTextEntry={true}
              type="text"
              placeholderTextColor={color.white}
              style={styles.textinput}
              value={password}
              onChangeText={(event) => this.setState({ password: event })}
            >

            </TextInput>


          </View>

          <View style={{ alignItems: 'center', justifyContent: 'center', marginBottom: normalize(30) }}>

            <TouchableOpacity
              style={styles.login_button}
              onPress={() => this._onPressButton()}
            >

              <Text style={{
                color: color.blue, fontSize: normalize(22), fontFamily: Fonts.MaliRegular
              }}>Login</Text>
            </TouchableOpacity>



            <View >

              <TouchableHighlight>
                <Text style={{ color: color.white, fontSize: normalize(20), fontFamily: Fonts.MaliRegular, marginTop: normalize(20) }}>Forgot your Password </Text>
              </TouchableHighlight>
            </View>


          </View>
        </View>
      </ImageBackground>
    )
  }
}

AppRegistry.registerComponent('AwesomeProject', () => UselessTextInput);
const styles = StyleSheet.create({

  button: {
    padding: 10,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    color: color.blue,
    borderWidth: normalize(2),
    borderColor: color.white,
    borderRadius: 15,
    backgroundColor: color.white,
    width: normalize(200),
    height: normalize(50),
    fontFamily: Fonts.MaliRegular
  },
  textinput: {
    width: normalize(350),
    height: normalize(60),
    padding: 10,
    borderBottomWidth: 1,
    borderColor: color.white,
    fontSize: normalize(20),
    fontFamily: Fonts.MaliRegular,
    color: color.white

  },
  login_button: {
    fontSize: normalize(30),
    width: normalize(350),
    height: normalize(60),
    padding: 7,
    borderRadius: 7,
    alignItems: 'center',
    backgroundColor: color.white,
    fontFamily: Fonts.MaliRegular,
    marginTop: normalize(20)


  },


});