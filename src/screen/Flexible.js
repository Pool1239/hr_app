import React, { Component } from 'react'
import { Text, View, StatusBar, StyleSheet, TouchableOpacity, TextInput, ImageBackground, Image } from 'react-native'
import color from '../color'
import { normalize } from '../function/nomalize';
import DatePicker from 'react-native-datepicker'
import { StackActions, NavigationActions } from 'react-navigation'
import Footer from '../component/Footer';
import image from '../resource/image'
import icon from '../resource/icon'
import Buttonapp from '../component/button/Buttonapp'
import Buttonnext from '../component/button/Buttonnext'
import { Select } from '../function/selectdata'
import { Fonts } from '../resource/Fonst'
import moment from 'moment'
export default class Flexible extends Component {

    constructor(props) {
        super(props)
        this.state = {
            date_start: moment().format("DD/MM/YYYY"),
            date_end: moment().format("DD/MM/YYYY"),
            flexiType: '',
            amount: ''

        }
    }


    _onback() {
        this.props.navigation.navigate("Home")
    }
    onSubmit = async () => {
        console.log("ส่งข้อมูล");

        this.props.navigate.navigate('')
    }
    render() {
        let data = [{
            value: 'Perpose for Flexible Benefit ',
            label: 'Perpose for Flexible Benefit '
        }, {
            value: 'Optical Purchasing',
            label: 'Optical Purchasing',
        },
        {
            value: 'Oral Purchasing',
            label: 'Oral Purchasing',
        },
        {
            value: 'Sprot',
            label: 'Sprot',
        },
        {
            value: 'Transportation on vacation',
            label: 'Transportation on vacation',
        },


        ];
        return (
            <View>


                <StatusBar translucent hidden />

                <ImageBackground source={image.backgroundhome} style={{ width: '100%', height: '100%' }} >

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: normalize(25), color: color.white, fontFamily: Fonts.MaliRegular, marginTop: normalize(60) }}> Flexible Benefit Request </Text>
                        <Image source={icon.user} style={{ width: normalize(60), height: normalize(60), borderRadius: 33, backgroundColor: color.white, marginTop: normalize(20) }}></Image>
                        <View style={{ backgroundColor: color.white, marginTop: 20, borderRadius: 7 }}>
                            <Select title={data} label={"Select ..."} />
                        </View>
                        <View style={{ alignItems: 'center' }}>

                            <View style={styles.date_input}>
                                <DatePicker
                                    style={styles.datepicker}
                                    format="DD-MM-YYYY"
                                    date={this.state.date_start}
                                    placeholder="select date"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 36
                                        }

                                    }}
                                    onDateChange={(date) => { this.setState({ date_start: date }) }}
                                />
                            </View>
                            <View style={styles.date_input}>
                                <DatePicker
                                    style={styles.datepicker}
                                    date={this.state.date_end}
                                    mode="date"
                                    placeholder="select date"
                                    format="DD-MM-YYYY"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                        dateIcon: {
                                            position: 'absolute',
                                            left: 0,
                                            top: 4,
                                            marginLeft: 0
                                        },
                                        dateInput: {
                                            marginLeft: 36
                                        }
                                        // ... You can check the source to find the other keys.
                                    }}
                                    onDateChange={(date) => { this.setState({ date_end: date }) }}
                                />

                            </View>
                        </View>

                        <View style={styles.text_input_area}>
                            <TextInput
                                placeholder="Amount"
                                placeholderTextColor='black'
                            ></TextInput>
                        </View>
                        <View style={{ flexDirection: 'row' }} >
                            <Buttonapp onBack={() => this._onback()} />
                            <Buttonnext onLogin={() => this.onSubmit()} />

                        </View>


                    </View>

                </ImageBackground>



                <Footer />

            </View >


        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: color.white,
        width: '100%',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: normalize(20),
        height: normalize(50),
        padding: 10,



    },
    button: {
        padding: 15,
        // textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        color: color.blue,
        borderWidth: normalize(2),
        borderColor: color.blue,
        borderRadius: 15,
        width: normalize(130),
        marginLeft: normalize(10),



    },
    text_input: {
        fontSize: normalize(20),
        marginTop: normalize(10),
        color: color.white,
        fontFamily: Fonts.MaliRegular
    },
    text_input_area: {
        borderRadius: normalize(5),
        width: normalize(300),
        height: normalize(60),
        padding: 10,
        marginTop: normalize(20),
        marginBottom: normalize(50),
        backgroundColor: color.white,
        alignItems: 'center'
    },

    date_input: {
        width: normalize(300),
        marginTop: normalize(20),

    },
    datepicker: {
        width: normalize(300),
        backgroundColor: color.white
    },
    flexible: {
        borderRadius: normalize(5),
        borderWidth: normalize(2),
        borderColor: color.bluesky,
        width: normalize(200),
        height: normalize(60),
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: normalize(30),
        borderLeftWidth: normalize(1),
        // display:'flex',
        // flexDirection:'row',
        marginBottom: normalize(200),
        backgroundColor: color.white
    },

    footer: {
        // position: 'absolute',
        bottom: 0,
        color: color.white,
        //  backgroundColor:color.bluesky,

    }
})