import React, { Component } from 'react'
import { Text, View, TextInput, ImageBackground, Alert, StyleSheet, TouchableOpacity, StatusBar } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation'
import image from '../resource/image'
import color from '../color'
import { normalize, width } from '../function/nomalize';
import Buttonapp from '../component/button/Buttonapp'
import Buttonnext from '../component/button/Buttonnext';
import { Fonts } from '../resource/Fonst'
import { register } from '../resource/constants'
import { POST, GET, Signupinsert } from '../service'

export default class Signup extends Component {
    constructor() {
        super()
        this.state = {
            username: '',
            password: '',
            emp_name: '',
            emp_last: '',
            confirmpassword: ''
        }
    }


    _ongotologin = () => {
        this.props.navigation.navigate('Login')
    }

    _ongotobackhome = () => {
        let { dispatch } = this.props.navigation;
        const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [
                NavigationActions.navigate({ routeName: 'Homestart' })
            ]
        })
        dispatch(resetAction)
    }

    onInsertSignup = async () => {


        if (this.state.password === this.state.confirmpassword) {

            const obj = {
                username: this.state.username,
                password: this.state.password,
                emp_name: this.state.emp_name,
                emp_last: this.state.emp_last,
            }
            console.log(obj)
            try {
                let res = await POST(Signupinsert, obj);
                console.log(res);
                console.log("ส่งข้อมูลสำเร็จ")
                this.props.navigation.navigate('Login')
            } catch (error) {
                console.log(error);

            }
        } else {
            this.testalert()
        }

    }
    testalert() {
        Alert.alert(
            'รหัสผ่านไม่ตรงกัน',
            'กรุณากรอกรหัสผ่านให้ตรงกัน...',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        );
    }
    disonchange = (e, ref) => {
        console.log("text", e);
        this.setState({ [ref]: e })


    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar translucent hidden />
                <ImageBackground source={image.backgroundhome} style={{ width: '100%', height: '100%' }} blurRadius={2}>
                    <View style={{ marginTop: normalize(30) }}>
                        <Text style={styles.text_title}> SIGN UP </Text>
                        <View style={{ marginTop: normalize(30), height: '50%' }}>
                            {this.Signup_input()}
                        </View>

                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity style={styles.buttonsubmit} onPress={() => this.onInsertSignup()}>
                            <Text style={styles.text_buttonsubmit}>ตกลง</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </View>
        )
    }
    Signup_input = () => {
        return (<View style={{ width, paddingHorizontal: normalize(20) }}>

            {this.renderTextInput()}


        </View>
        )
    }

    renderTextInput() {
        return register.map((el, i) => {
            let { ref, label, placeholder } = el
            return <View style={{ flexDirection: 'row', alignItems: 'center' }} key={i}>
                <Text style={styles.text_input}>{label} </Text>
                <TextInput style={styles.border_textinput} key={ref} placeholder={placeholder} onChangeText={e => this.disonchange(e, ref)} placeholderTextColor={color.white} />
            </View>
        })
    }
}





const styles = StyleSheet.create({
    text_title: {
        fontSize: normalize(50),
        color: color.white,
        fontFamily: Fonts.MaliRegular

    },
    text_input: {
        fontSize: normalize(20),
        flex: 1,
        color: color.white,
        fontFamily: Fonts.MaliRegular


    },
    border_textinput: {
        flex: 2,
        borderBottomWidth: 1,
        borderColor: color.white,
        fontFamily: Fonts.MaliRegular,
        paddingVertical: normalize(6),
        color: color.white

    },
    buttonsubmit: {
        width: normalize(330),
        height: normalize(50),
        backgroundColor: color.white,
        borderRadius: normalize(7),
        alignItems: 'center',
        padding: normalize(5),
        marginBottom: 0
    },
    text_buttonsubmit: {
        fontSize: normalize(20),
        color: color.blue,
        fontFamily: Fonts.MaliRegular,
        alignItems: 'center'

    }

})