import React, { Component } from 'react'
import { Text, View, ImageBackground, StyleSheet, TouchableOpacity, TextInput, StatusBar, Image, ScrollView } from 'react-native'
import color from '../color'
import { normalize } from '../function/nomalize';
import DatePicker from 'react-native-datepicker'
import { StackActions, NavigationActions } from 'react-navigation'
import Footer from '../component/Footer'
import image from '../resource/image';
import icon from '../resource/icon'
import Buttonapp from '../component/button/Buttonapp'
import Buttonnext from '../component/button/Buttonnext';
import { Select } from '../function/selectdata'
import { Fonts } from '../resource/Fonst'
import RadioGroup from 'react-native-radio-buttons-group';
import moment from 'moment'
import { getNavigationParams } from '../function/function';
import { Selecttime_start } from '../function/selectdatetime'
import { POST, Insert_Leave } from '../service';


export default class Leaverequest extends Component {
  constructor(props) {
    super(props)
    this.state = {
      lea_type: 'Fullday',
      date_start: moment().format("DD-MM-YYYY"),
      date_end: moment().format("DD-MM-YYYY"),
      lea_reason: '',
      lea_day: 'Morning',
      radiobox: [],
      time_start: '',
      time_end: '',





    }
  }
  componentWillMount = () => {
    let el = getNavigationParams(this.props) // เขียนแบบฟังก์ชั่น
    console.log(el);


  }
  // componentDidUpdate() {
  //   if (this.state.lea_type === "Hafttime") {
  //     this.setState({
  //       // lea_day: '',
  //       // date_start: moment().format("DD-MM-YYYY"),
  //       // date_end: moment().format("DD-MM-YYYY"),
  //       lea_reason: '555',
  //       // time_start: '',
  //       // time_end: ''

  //     })
  //     console.log("after", lea_reason);
  //   }


  // }
  onsubmit = async () => {
    let el = getNavigationParams(this.props) // เขียนแบบฟังก์ชั่น
    const obj = {
      lea_type: el.lea_type,
      lea_day: this.state.lea_type === "Fullday" ? 1 : 2,
      date_start: this.state.date_start,
      date_end: this.state.date_end,
      lea_reason: this.state.lea_reason,
      // time_start: this.state.time_start,
      // time_end: this.state.time_end,
      empID: el.empID

    }
    console.log(obj);

    try {
      let res = await POST(Insert_Leave, obj);
      console.log(res);

    } catch (error) {

    }

  }

  _onback = () => {
    this.props.navigation.goBack(null)
  }

  onValueChange = (value) => {
    this.setState({ lea_type: value }, () => console.log(this.state.lea_type))
  }



  setdateTime = (ref, date) => {
    this.setState({ [ref]: date })
    console.log("date", date);

  };

  onPress = (dataradio) => {

    let index = dataradio.find(el => el.selected === true)

    // console.log(index.label);
    let lea_day = index.label
    this.setState({ lea_day: lea_day })
    console.log(lea_day);


  };

  render() {
    const data = [
      {
        label: 'Fullday',
        value: 'Fullday',

      },
      {
        label: 'Haftday',
        value: 'Haftday',

      },
    ];


    let el = getNavigationParams(this.props) // เขียนแบบฟังก์ชั่น

    return (
      <View style={{ flex: 1 }}>
        <View style={{ backgroundColor: '#003049' }}>
          <StatusBar translucent barStyle="light-content" />
          {/* <ImageBackground source={image.backgroundhome} style={{ width: '100%', height: '100%' }} >ฝ */}
          <View style={{ justifyContent: 'center', alignItems: 'center', margin: 20 }}>
            <Text style={{ fontSize: normalize(25), color: color.title, fontFamily: Fonts.MaliRegular, marginTop: normalize(40) }}> Leave Request  </Text>
            <Image source={icon.user} style={{ width: normalize(60), height: normalize(60), borderRadius: 33, backgroundColor: color.white, marginTop: normalize(20) }}></Image>

            <View style={{ alignItems: 'center' }}>
              <Text style={styles.text_input}>ประเภทการลา :{el.leaName_type}</Text>
            </View>
            {/* <ScrollView> */}
            <View style={{ alignItems: 'center' }}>
              <View style={{ marginTop: 20, borderRadius: 7 }}>
                <Select title={data} label={"Select ..."} state={this.state.lea_type} onValueChange={this.onValueChange} />
              </View>

              {this.typeleave()}
            </View>


            <View style={{ alignItems: 'center' }}>


              <View style={styles.text_input_area}>
                <TextInput
                  style={{ fontFamily: Fonts.MaliRegular, fontSize: normalize(15), padding: normalize(20) }}
                  onChangeText={text => this.setState({ lea_reason: text })}
                  placeholder="Leave Reason..."
                  placeholderTextColor='black'
                ></TextInput>
              </View>
            </View>

            <View style={{ margin: normalize(20), marginBottom: normalize(300) }}>
              <Buttonnext onLogin={() => this.onsubmit()} />
              <Buttonapp onBack={() => this._onback()} />
            </View>


            {/* </ScrollView> */}
          </View>








        </View>
        {/* <Footer /> */}
      </View >
    )
  }




  typeleave() {

    const dataradio = [

      {
        // disabled: this.state.lea_type ? true : false,
        label: 'Morning',
        value: 'Morning',
        color: color.blue,
      },
      {
        // disabled: this.state.lea_type ? true : false,
        label: 'Afternoon',
        color: color.blue,
        value: 'Afternoon',
      },
    ]
    return <View>

      {
        this.state.lea_type === "Fullday" ?
          < View style={{ margin: normalize(20) }}>
            <View style={styles.date_input}>
              <DatePicker
                style={styles.datepicker}
                format="DD-MM-YYYY"
                date={this.state.date_start}
                placeholder="select date"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 36
                  }

                }}
                onDateChange={(date) => { this.setState({ date_start: date }) }}
              />
            </View>
            <View style={styles.date_input}>
              <DatePicker
                style={styles.datepicker}
                date={this.state.date_end}
                mode="date"
                placeholder="select date"
                format="DD-MM-YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                }}
                onDateChange={(date) => { this.setState({ date_end: date }) }}
              />

            </View>
          </View>
          : this.state.lea_type === "Haftday" ?
            <View style={{ margin: normalize(20) }}>

              <View style={styles.container}>

                <RadioGroup radioButtons={dataradio} onPress={this.onPress} />
                <View style={{ textAlign: 'right' }}>
                  <Text style={styles.valueText}>{` เวลาเริ่มต้น :  ${this.state.time_start}`}</Text>
                  <Text style={styles.valueText}> {` เวลาสิ้นสุด :  ${this.state.time_end}`}</Text>
                </View>
                {
                  this.state.lea_day ?
                    <View style={{ flexDirection: 'row', margin: normalize(10) }}>
                      <Selecttime_start name={'time_start'} setdateTime={(ref, data) => this.setdateTime(ref, data)} title={"เวลาเริ่มต้น"} />
                      <Selecttime_start name={'time_end'} setdateTime={(ref, data) => this.setdateTime(ref, data)} title={"เวลาสิ้นสุด"} />
                    </View>
                    : null
                }

              </View>

            </View>
            : null
      }
    </View>
  }


}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: color.white,
    width: normalize(330),
  },
  valueText: {
    fontSize: normalize(20),
    margin: normalize(10),
    color: color.blue,
    fontFamily: Fonts.MaliRegular,

  },
  title: {
    color: color.white,
    width: '100%',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: normalize(20),
    height: normalize(50),
    padding: 10,



  },
  button: {
    padding: 15,
    // textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    color: color.blue,
    borderWidth: normalize(2),
    borderColor: color.blue,
    borderRadius: 15,
    width: normalize(130),
    marginLeft: normalize(10),



  },
  text_input: {
    fontSize: normalize(20),
    marginTop: normalize(10),
    color: color.title,
    fontFamily: Fonts.MaliRegular,
    width: normalize(300)
  },
  text_input_area: {
    borderRadius: normalize(5),
    width: normalize(330),
    height: normalize(80),
    backgroundColor: color.white,
    alignItems: 'center',
    borderWidth: 1
  },

  date_input: {
    width: normalize(330),
    marginTop: normalize(20),

  },
  datepicker: {
    width: normalize(330),
    backgroundColor: color.white
  },
  flexible: {
    borderRadius: normalize(5),
    borderWidth: normalize(2),
    borderColor: color.bluesky,
    width: normalize(200),
    height: normalize(60),
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: normalize(30),
    borderLeftWidth: normalize(1),
    // display:'flex',
    // flexDirection:'row',
    marginBottom: normalize(200),
    backgroundColor: color.white
  },

  footer: {
    // position: 'absolute',
    bottom: 0,
    color: color.white,
    //  backgroundColor:color.bluesky,

  }
})