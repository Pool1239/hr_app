import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, ScrollView, TouchableOpacity, StatusBar, ImageBackground, ActivityIndicator, RefreshControl } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation'
import { normalize } from '../function/nomalize';
import color from '../color'
import image from '../resource/image'
import icon from '../resource/icon'
import Footer from '../component/Footer'
import { POST, Public, GET, GET_profile } from '../service';
import { Fonts } from '../resource/Fonst';
import moment from 'moment'



export default class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      IDcompany: null,
      leaID_type: null,
      leaName_thai: null,
      leaName_type: null,
      max_day: null,
      box_leavearray: [],
      refreshing: false,

    }
  }



  _onselectleave = (el) => {
    console.log(el);

    let { leaName_type, empID, lea_day, lea_type } = el
    this.props.navigation.navigate('Leave',
      { leaName_type, empID, lea_day, lea_type } // ส่งตัวแปรไปอีกหน้า *** object เท่านั้น
    )

  }

  componentDidMount = async () => {

    this.onRefresh()
    try {
      let res = await GET(Public);
      if (res) {
        // console.log(res.result)
        this.setState({ box_leavearray: res.result })
      }
      // console.log("box_arrya", this.state.box_leavearray)
    } catch (error) {
      console.log(error)
    }
  }

  componentWillMount = () => {
    this.getprofiledata()
    this._onbox_leaveboxmap()
  }
  getprofiledata = async () => {
    let p = this.props.navigation.state.params
    let empID = p.empID
    let res = await POST(GET_profile, { empID });
    if (res.success === true) {
      // console.log("profile", res.result);
      res.result.map((el) => {
        this.setState({ emp_name: el.emp_name, emp_last: el.emp_last })
      })
    }
  }


  _onselectflexible = () => {
    this.props.navigation.navigate("Flex")
  }
  Logout = () => {

    let { dispatch } = this.props.navigation;
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: 'Login' })
      ]
    })
    dispatch(resetAction)
  }




  onFetchApi() {

    this.setState({ refreshing: false })
  }

  onRefresh() {
    this.setState({ refreshing: true })
    setTimeout(() => {
      this.onFetchApi()
    }, 1000)
  }


  render() {
    const { refreshing, emp_name, emp_last } = this.state
    return (
      <View >
        <ImageBackground source={image.backgroundhome} style={{ width: '100%', height: '100%' }} >
          <View style={{ height: '100%', width: '100%' }}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <StatusBar translucent barStyle={'light-content'} backgroundColor={'transparent'} />
              <View style={{ alignItems: 'center' }}>
                <Text style={{ fontSize: normalize(25), color: color.white, fontFamily: Fonts.MaliRegular, marginTop: normalize(60) }}> Leave Request & Remaining </Text>
                <View style={{ flexDirection: 'row' }}>
                  <Image source={icon.user} style={{ width: normalize(60), height: normalize(60), borderRadius: 33, backgroundColor: color.white, marginTop: normalize(40) }}></Image>
                  <Text style={{ color: color.title, fontFamily: Fonts.MaliRegular, fontSize: normalize(20), paddingTop: 40 }}> {` ชื่อ : ${emp_name} นามสกุล : ${emp_last}`}</Text>
                </View>

              </View>

            </View>
            <Text> {this.state.empID} </Text>

            <View style={{ alignItems: 'center', flex: 1 }}>

              <Text style={{ color: color.title, fontSize: normalize(20), fontFamily: Fonts.MaliRegular }}>Public Holiday</Text>


              <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => this.onRefresh()} />}>
                <View>{this.leavedata()}</View>
              </ScrollView>
            </View>




            <View style={{ marginTop: normalize(30), alignItems: 'center', marginBottom: normalize(60) }}>
              <View style={{ flexDirection: 'row' }}>
                <Image source={icon.flex} style={{ width: normalize(40), height: normalize(40) }} ></Image>
                <TouchableOpacity style={styles.flexible} onPress={() => this._onselectflexible()}>
                  <View style={{ alignItem: 'center', flexDirection: 'row' }}>

                    <Text style={styles.text_flex} >คำขอเกี่ยวกับสวัสดิการอื่นๆ</Text>
                    <Image source={icon.pointright} style={{ width: normalize(10), height: normalize(10), marginTop: normalize(6) }} ></Image>
                  </View>

                </TouchableOpacity>
              </View>

            </View>

          </View>

        </ImageBackground>
        <Footer />
      </View>




    )
  }

  leavedata = () => {
    return (<View style={{ paddingHorizontal: normalize(20) }}>

      {this._onbox_leaveboxmap()}



    </View>
    )
  }

  _onbox_leaveboxmap() {
    return this.state.box_leavearray.map((el, i) => {
      let calculatedate = parseInt(moment(el.date_end).format("DD")) - parseInt(moment(el.date_start).format("DD"))


      let { date_start, date_end } = el
      return (
        <View style={{ alignItems: 'center', marginTop: normalize(20), width: '100%' }} key={i}>
          <TouchableOpacity key={i} onPress={() => this._onselectleave(el)} style={{ width: '100%', alignItems: 'center', borderRadius: normalize(33) }}>
            <View style={{ alignItems: 'center', width: normalize(340), height: normalize(95), backgroundColor: color.white, borderRadius: 10 }}>
              <Text style={styles.text} >{el.leaName_type}</Text>
              <Text style={styles.text} >{el.leaName_thai}</Text>
              <Text style={styles.text} >{` ${calculatedate} / ${el.max_day} วัน`}</Text>
            </View>
          </TouchableOpacity>
        </View>
      )


    })
  }

}


const styles = StyleSheet.create({
  title: {
    color: color.white,
    width: '100%',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: normalize(20),
    height: normalize(50),
    padding: 10,
    marginTop: normalize(50)



  },
  leave: {
    borderRadius: normalize(5),
    borderWidth: normalize(2),
    borderColor: color.bluesky,
    width: '100%',
    height: normalize(100),
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftWidth: normalize(1),
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: color.white
  },
  flexible: {
    borderRadius: normalize(30),
    width: normalize(300),
    height: normalize(40),
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color.orenge
  },

  text: {
    fontSize: normalize(15),
    color: color.black,
    fontFamily: Fonts.MaliRegular,
    // padding: 5

  },
  text_flex: {
    fontFamily: Fonts.MaliRegular,
    fontSize: normalize(15),
    color: color.white,



  }
})