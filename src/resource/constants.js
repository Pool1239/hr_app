export const register = [

    {
        label: 'Name',
        placeholder: 'กรุณากรอกชื่อ',
        ref: 'emp_name'
    },
    {
        label: 'Last',
        placeholder: 'กรุณากรอกนามสกุล',
        ref: 'emp_last'
    },
    {
        label: 'Username',
        placeholder: 'กรุณากรอกชื่อ',
        ref: 'username'
    },
    {
        label: 'Password',
        placeholder: 'กรุณากรอกรหัสผ่าน',
        ref: 'password'
    }, {
        label: 'ConfrimPassword',
        placeholder: 'กรุณากรอกรหัสผ่านอีกครั้ง',
        ref: 'confirmpassword'
    }


]