export default {
    user: require('../resource/icon/user.png'),
    flex: require('../resource/icon/flexible.png'),
    namelave: require('../resource/icon/namelave.png'),
    typelave: require('../resource/icon/typelave.png'),
    userlogout: require('../resource/icon/userlogout.png'),
    leftpoint: require('../resource/icon/back.png'),
    pointingleft: require('../resource/icon/pointingleft.png'),
    pointingtoleft: require('../resource/icon/pointingtoleft.png'),
    pointright: require('../resource/icon/pointright.png'),
    muscod: require('../resource/icon/iconmuscod.png'),
}