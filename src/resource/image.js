export default {
    logo: require('./image/logo.png'),
    logo1: require('./image/logo2.jpg'),
    logo2: require('./image/logo1.png'),
    logo3: require('./image/logo2.png'),
    home1: require('./image/home1.jpg'),
    home2: require('./image/home2.jpg'),
    background: require('./image/background.jpg'),
    background1: require('./image/background1.jpg'),
    background2: require('./image/background2.png'),
    backgroundhome: require('./image/backgroundhome.png'),
    backgroundleave: require('./image/backgroundleave.jpg'),
    backgroundbuile: require('./image/backgroundbuile.jpg'),
    backgroundbuile1: require('./image/1.jpg'),

}
