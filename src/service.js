import apisauce from 'apisauce';


export const ip = 'http://192.168.1.120:3001/api/v1';
const api = apisauce.create({
    baseURL: ip,
    headers: {
        "Content-Type": "application/json"
    },
    withCredentials: true
})
export const POST = (path, obj) =>
    new Promise((resolve, reject) => {
        api.post(path, obj)
            .then(response => {
                if (response.ok) {
                    resolve(response.data)
                } else {
                    response.data ? reject(response.data) : reject({ success: false, message: response.problem })
                }

            }).catch(err => reject(err))
    })

export const GET = (path) =>
    new Promise((resolve, reject) => {
        api.get(path)
            .then(response => {
                if (response.ok) {
                    resolve(response.data)
                } else {
                    response.data ? reject(response.data) : reject({ success: false, message: response.problem })
                }

            }).catch(err => reject(err))
    })


export const LOGIN = '/login_web'
export const Public = '/get_plubic'
export const Signupinsert = '/registeruser'
export const Insert_Leave = '/insert_leave'
export const GET_profile = '/getprofiledata'