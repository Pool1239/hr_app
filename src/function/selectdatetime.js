import React, { Component } from 'react'
import { Text, TouchableOpacity, View, StyleSheet } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment'
import { Fonts } from '../resource/Fonst';
import { normalize } from '../function/nomalize';
import color from '../color'



export class Selecttime_start extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isDateTimePickerVisible: false
        }
    }

    componentWillMount = () => {
        console.log(this.props.title);

    }


    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log(moment(date).format('LTS'));
        this.props.setdateTime(this.props.name, moment(date).format('LTS'))
        this._hideDateTimePicker();
    };
    render() {
        let { title } = this.props;
        return <View>
            <View style={{}}>
                <TouchableOpacity onPress={this._showDateTimePicker} style={styles.buttontime}>
                    <Text style={{ fontFamily: Fonts.MaliRegular, fontSize: normalize(15), color: color.white }}>{title}</Text>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    mode="time"
                />
            </View>
        </View>
    }
}

const styles = StyleSheet.create({
    buttontime: {
        width: normalize(100),
        padding: normalize(10),
        borderRadius: 7,
        backgroundColor: "#d62828",
        margin: normalize(10)
    }
})