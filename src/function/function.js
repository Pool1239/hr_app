
export function getNavigationParams({ navigation }) {
    let { params } = navigation.state
    return params || {}
}