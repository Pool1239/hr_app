
import React, { Component } from 'react';
import { normalize } from '../function/nomalize';
import color from '../color'
import { Fonts } from '../resource/Fonst';
import { StyleSheet, View, Text } from 'react-native'
import RNPickerSelect from 'react-native-picker-select';




export const Select = ({ title, label, state, onValueChange }) => {
    return <View style={{ width: normalize(330) }}>
        <RNPickerSelect
            placeholder={{
                label: label,
                value: null,
                color: '#9EA0A4',
            }}
            items={title}
            onValueChange={(value) => {
                onValueChange(value)
            }}
            onUpArrow={() => {
                this.inputRefs.name.focus();
            }}
            onDownArrow={() => {
                this.inputRefs.picker2.togglePicker();
            }}
            style={{ ...pickerSelectStyles }}
        // value={this.state.favColor}
        // ref={(el) => {
        //     this.inputRefs.picker = el;
        // }}
        />

    </View>




}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: normalize(16),
        paddingTop: normalize(13),
        paddingHorizontal: normalize(10),
        paddingBottom: normalize(12),
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
        fontFamily: Fonts.MaliRegular,
        alignItems: 'center'

    },
    inputAndroid: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});