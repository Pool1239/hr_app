export default {
    red: '#ff0000',
    blue: '#0099ff',
    bluesky: '#0099FF',
    black: '#000000',
    white: '#ffffff',
    blue1: '#0040ff',
    gray: '#adad85',
    buttonlogin: '#0099ff',
    buttonnext: '#33d6ff',
    orenge: '#FF8C00',
    yellow: '#ffff00',
    back: '#fcbf49',
    next: '#f77f00',
    title: '#eae2b7'


}