import React from 'react'
import { Text, View } from 'react-native'
import { createStackNavigator, createDrawerNavigator, } from 'react-navigation'
import Login from './src/screen/Login'
import Home from './src/screen/Home'
import Leaverequest from './src/screen/Leaverequest';
import Flexible from './src/screen/Flexible'
import { HeaderBack } from './src/component/Header';
import Homestart from './src/screen/Homestart'
import Signup from './src/screen/Signup'
import Loginfacebook from './src/screen/Loginfacebook'
import { HeaderTitle, HeaderRight } from './src/component/Header'
import color from './src/color'



export const navigationOptions = {
    headerStyle: {
        backgroundColor: color.white,
    },
    headerTitleStyle: {
        alignItems: 'center',
        textAlign: 'center',
        alignSelf: 'center',
    },
    headerTextStyle: {
        textAlign: 'center',
        flexGrow: 1
    },
}


const HomeStack = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: { header: null }

    },

    Leave: {
        screen: Leaverequest,
        navigationOptions: { header: null }

    },
    Flex: {
        screen: Flexible,
        navigationOptions: { header: null }
    }

})

// const HomeDrawer = createDrawerNavigator({
//     Home: {
//         screen: HomeStack,

//     },

// })
const Homeapp = createStackNavigator({
    Homestart: {
        screen: Homestart,
        navigationOptions: { header: null }

    },
    Signup: {
        screen: Signup,
        navigationOptions: {


        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            ...navigationOptions,
            headerTitle: <HeaderTitle title={'หน้าเข้าสู่ระบบ'} />,
            headerRight: <HeaderRight />,

        }
    },
    Home: {
        screen: HomeStack,
        navigationOptions: {
            header: null

        }
    }
})


// MAIN
export default createStackNavigator({
    Homestart: {
        screen: Homeapp,
        navigationOptions: { header: null },
    },


})